'use strict';

const countries = require('../data/countries.json');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Countries', countries);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Countries');
  }
};
