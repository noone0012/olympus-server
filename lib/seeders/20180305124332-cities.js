'use strict';

const cities = require('../data/cities.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Cities', cities);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Cities');
  }
};
