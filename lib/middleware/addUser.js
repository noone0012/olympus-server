import jwt from 'jsonwebtoken';

const addUser = () => {
  return async (ctx, next) => {
    try {
      const token = ctx.request.get('x-access-token');
      const payload = jwt.verify(token, process.env.ACCESS_SECRET);

      ctx.state.user = payload.user;
    } catch (e) {}

    await next();
  };
};

export default addUser;
