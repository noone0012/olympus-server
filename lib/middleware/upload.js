import formidable from 'formidable';
import fs from 'fs';

const upload = () => {
  return async (ctx, next) => {
    if (!ctx.is('multipart/form-data')) {
      return await next();
    }

    const form = new formidable.IncomingForm();
    form.multiples = true;

    ctx.req.body = ctx.request.body;

    const parsing = () =>
      new Promise(resolve => {
        form.parse(ctx.req, async (err, fields, { files }) => {
          files = files || [];

          if (!Array.isArray(files)) {
            files = [files];
          }

          ctx.request.body = JSON.parse(fields.operation);
          ctx.request.body.variables.input.files = files.map(file => ({
            ext: file.type.replace(/^.*?\//, ''),
            type: file.type,
            path: file.path
          }));

          resolve();
        });
      });

    await parsing();
    await next();

    ctx.request.body.variables.input.files.forEach(file =>
      fs.unlink(file.path)
    );
  };
};

export default upload;
