import models from '../models';

const formatErrors = errors => {
  console.log(errors);
  if (errors instanceof models.sequelize.ValidationError) {
    return errors.errors.map(err => ({
      key: err.path,
      message: err.message
    }));
  }

  if (Array.isArray(errors)) {
    return errors.map(({ key, message }) => {
      if (typeof key === 'string' && typeof message === 'string') {
        return {
          key,
          message
        };
      }
    });
  }

  return [
    {
      key: errors.key || '_error',
      message: errors.message || 'Something went wrong'
    }
  ];
};

export default formatErrors;
