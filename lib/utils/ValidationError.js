import { GraphQLError } from 'graphql';

class ValidationError extends GraphQLError {
  constructor(errors) {
    super('Error');

    this.state = errors.reduce((result, { key, message }) => {
      if (!result[key]) {
        result[key] = [];
      }

      result[key].push(message);
      return result;
    }, {});
  }
}

export default ValidationError;
