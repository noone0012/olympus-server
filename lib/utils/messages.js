export const WRONG_EMAIL =
  "The email you entered doesn't belong to an account. Please check your email and try again.";

export const WRONG_PASSWORD =
  'Sorry, your password was incorrect. Please double-check your password.';

// ----------------------

export const LENGTH_RANGE = (a, b) =>
  `Must be between ${a} and ${b} characters long.`;

export const MAX_LENGTH = a => `Must be at most ${a} characters long.`;
