export const greaterThen16 = value => {
  const currentYear = new Date().getFullYear();
  const birthYear = new Date(value).getFullYear();

  if (currentYear - birthYear < 16) {
    throw new Error('You must be over the age of 16.');
  }
};
