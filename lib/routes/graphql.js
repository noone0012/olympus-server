import koaRouter from 'koa-router';
import { graphqlKoa, graphiqlKoa } from 'apollo-server-koa';
import DataLoader from 'dataloader';

import schema from '../schema';
import models from '../models';
import { PORT } from '../index';

const router = new koaRouter();

const { User, Sequelize: { Op } } = models;

const loaders = {
  userLoader: new DataLoader(ids =>
    User.findAll({
      where: {
        id: { [Op.in]: ids }
      }
    }).then(users =>
      ids.reduce((a, b) => a.concat(users.find(user => user.id === b)), [])
    )
  )
};

router.post(
  '/graphql',
  graphqlKoa(ctx => {
    return {
      schema,
      context: {
        models,
        user: ctx.state.user,
        loaders,
        host: ctx.host
      },
      formatError: error => {
        return {
          ...error,
          state: error.originalError && error.originalError.state
        };
      }
    };
  })
);
router.get(
  '/graphiql',
  graphiqlKoa({
    endpointURL: '/graphql',
    subscriptionsEndpoint: `ws://localhost:${1200}/subscriptions`
  })
);

export default router;
