import koaRouter from 'koa-router';
import jwt from 'jsonwebtoken';

import models from '../models';

const router = new koaRouter();

router.get('/confirmation/:token', async (ctx, next) => {
  const { token } = ctx.params;
  const { user: { id } } = jwt.verify(token, process.env.EMAIL_SECRET);

  const user = await models.User.findById(id);

  if (!user.confirmed) {
    user.confirmed = true;
    await user.save();

    // redirect to login page
    ctx.redirect(`http://localhost:3000/login`);
  }
});

export default router;
