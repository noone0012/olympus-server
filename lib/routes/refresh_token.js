import koaRouter from 'koa-router';

import { refreshToken } from '../service/auth';
import models from '../models';

const router = new koaRouter();

router.get('/refresh_token', async ctx => {
  try {
    const token = ctx.request.get('x-refresh-token');

    const accessToken = await refreshToken({
      token,
      models
    });

    ctx.body = { accessToken };
  } catch (e) {
    ctx.body = { message: 'Refresh token is invalid.' };
    ctx.status = 401;
  }
});

export default router;
