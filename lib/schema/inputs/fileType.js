import { GraphQLInputObjectType, GraphQLNonNull, GraphQLString } from 'graphql';

const fileType = new GraphQLInputObjectType({
  name: 'File',
  fields: {
    type: { type: new GraphQLNonNull(GraphQLString) },
    path: { type: new GraphQLNonNull(GraphQLString) },
    ext: { type: new GraphQLNonNull(GraphQLString) }
  }
});

export default fileType;
