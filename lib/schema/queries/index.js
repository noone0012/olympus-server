import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLBoolean,
  GraphQLID
} from 'graphql';

import {
  fromGlobalId,
  connectionArgs,
  connectionFromArray
} from 'graphql-relay';

import userType, { userDefinitions } from '../types/userType';
import viewerType from '../types/viewerType';
import rootType from './rootType';
import { isAuthenticated } from '../resolvers';
import { nodeField, nodesField } from '../node';

nodeField.resolve = isAuthenticated.createResolver(nodeField.resolve);

export default new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    node: nodeField,
    nodes: nodesField,
    viewer: {
      type: new GraphQLNonNull(viewerType),
      description: 'Fetches current logged in user',
      resolve: isAuthenticated.createResolver(
        (obj, args, { models: { User }, user }) => {
          return User.findByIdAndCheck(user.id);
        }
      )
    },
    root: {
      type: new GraphQLNonNull(rootType),
      description: 'Connection fields',
      resolve: isAuthenticated.createResolver(() => ({}))
    }
  })
});
