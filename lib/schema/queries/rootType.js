import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLID,
  GraphQLList
} from 'graphql';
import {
  globalIdField,
  connectionArgs,
  connectionFromArray,
  fromGlobalId
} from 'graphql-relay';

import { messageDefinitions } from './../types/messageType';
import { nodeInterface } from '../node';
import userType, { userDefinitions } from '../types/userType';
import countryType from '../types/countryType';
import cityType from '../types/cityType';
import {
  getPagingParameters,
  connectionFromPaginatedResult
} from '../connections';
import type { PagingParameters } from '../connections';
import {
  getUsers,
  getMessages
} from '../../service/rawQueries';

const rootType = new GraphQLObjectType({
  name: 'Root',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID),
      resolve() {
        return 'root_connections';
      }
    },
    countries: {
      type: new GraphQLList(new GraphQLNonNull(countryType)),
      description: 'List of all countries',
      resolve(obj, args, { models: { Country } }) {
        return Country.findAll({ raw: true });
      }
    },
    cities: {
      type: new GraphQLList(new GraphQLNonNull(cityType)),
      description: 'Fetches cities of specific country',
      args: { countryId: { type: new GraphQLNonNull(GraphQLID) } },
      resolve(obj, { countryId }, { models: { City } }) {
        return City.findAll({
          where: { countryId: fromGlobalId(countryId).id },
          raw: true
        });
      }
    },
    messages: {
      type: messageDefinitions.connectionType,
      args: {
        ...connectionArgs,
        userId: { type: new GraphQLNonNull(GraphQLID) }
      },
      async resolve(obj, args, { user }) {
        const otherId = fromGlobalId(args.userId).id;

        const pagingParameters: PagingParameters = getPagingParameters(args);

        const messages = await getMessages(
          {
            viewerId: user.id,
            peerId: otherId
          },
          pagingParameters
        );

        return connectionFromPaginatedResult(messages, pagingParameters);
      }
    },
    users: {
      type: userDefinitions.connectionType,
      args: {
        ...connectionArgs,
        fullName: { type: GraphQLString, defaultValue: '' }
      },
      async resolve(obj, args, { models: { User, Sequelize: { Op } } }) {
        const [firstName = '', lastName = ''] = args.fullName
          .split(' ')
          .filter(v => v !== '');

        const pagingParameters: PagingParameters = getPagingParameters(args);

        const users = await getUsers(
          {
            firstName: `${firstName}%`,
            lastName: `${lastName}%`
          },
          pagingParameters
        );

        return connectionFromPaginatedResult(users, pagingParameters);
      }
    }
  })
});

export default rootType;
