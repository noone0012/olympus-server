import { nodeDefinitions, fromGlobalId } from 'graphql-relay';
import models from '../models';

const getObjectFromGlobalId = globalId => {
  try {
    let { type, id } = fromGlobalId(globalId);

    if (type === 'Viewer') {
      type = 'User';
    }

    const Model = models[type];

    return Model.findById(id);
  } catch (e) {
    throw new Error('Your provided ID is invalid.');
  }
};

const sequelizeTypeToGraphQLType = obj => {
  const userType = require('./types/userType').default;
  const cityType = require('./types/cityType').default;
  const countryType = require('./types/countryType').default;
  const careerType = require('./types/careerType').default;
  const albumType = require('./types/albumType').default;
  const photoType = require('./types/photoType').default;
  const messageType = require('./types/messageType').default;
  const friendshipType = require('./types/friendshipType').default;

  if (obj instanceof models.User) {
    return userType;
  }

  if (obj instanceof models.City) {
    return cityType;
  }

  if (obj instanceof models.Country) {
    return countryType;
  }

  if (obj instanceof models.Career) {
    return careerType;
  }

  if (obj instanceof models.Album) {
    return albumType;
  }

  if (obj instanceof models.Photo) {
    return photoType;
  }

  if (obj instanceof models.Message) {
    return messageType;
  }

  if (obj instanceof models.Friendship) {
    return friendshipType;
  }

  return null;
};

export const { nodeInterface, nodeField, nodesField } = nodeDefinitions(
  getObjectFromGlobalId,
  sequelizeTypeToGraphQLType
);
