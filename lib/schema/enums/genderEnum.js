import { GraphQLEnumType } from 'graphql';

const genderEnum = new GraphQLEnumType({
  name: 'Gender',
  values: {
    MALE: { value: '0' },
    FEMALE: { value: '1' }
  }
});

export default genderEnum;
