import { GraphQLEnumType } from 'graphql';

const friendshipStatusEnum = new GraphQLEnumType({
  name: 'FriendshipStatus',
  values: {
    NOT_A_FRIEND: { value: '0' },
    REQUEST_WAS_SENT: { value: '1' },
    INCOMING_REQUEST: { value: '2' },
    IS_A_FRIEND: { value: '3' }
  }
});

export default friendshipStatusEnum;
