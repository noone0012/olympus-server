import { GraphQLEnumType } from 'graphql';

const friendAccessEnum = new GraphQLEnumType({
  name: 'FriendAccess',
  values: {
    ALL: { value: '0' },
    FRIENDS_OF_FRIENDS: { value: '1' }
  }
});

export default friendAccessEnum;
