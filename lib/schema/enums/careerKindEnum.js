import { GraphQLEnumType } from 'graphql';

const careerKindEnum = new GraphQLEnumType({
  name: 'CareerKind',
  values: {
    EDUCATION: { value: '0' },
    EMPLOYEMENT: { value: '1' }
  }
});

export default careerKindEnum;
