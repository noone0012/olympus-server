import { GraphQLEnumType } from 'graphql';

const ViewPostsAccessEnum = new GraphQLEnumType({
  name: 'ViewPostsAccess',
  values: {
    ALL: { value: '0' },
    FRIENDS_OF_FRIENDS: { value: '1' }
  }
});

export default ViewPostsAccessEnum;
