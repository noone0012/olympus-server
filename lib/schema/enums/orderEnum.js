import { GraphQLEnumType } from 'graphql';

const orderEnum = new GraphQLEnumType({
  name: 'OrderEnum',
  values: {
    ASC: {},
    DESC: {}
  }
});

export default orderEnum;
