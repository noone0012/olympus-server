import { GraphQLEnumType } from 'graphql';

const statusEnum = new GraphQLEnumType({
  name: 'Status',
  values: {
    SINGLE: { value: '0' },
    IN_A_RELATIONSHIP: { value: '1' },
    ENGAGED: { value: '2' },
    MARRIED: { value: '3' }
  }
});

export default statusEnum;
