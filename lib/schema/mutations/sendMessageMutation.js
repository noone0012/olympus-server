import { GraphQLNonNull, GraphQLString, GraphQLID } from 'graphql';
import { mutationWithClientMutationId, fromGlobalId } from 'graphql-relay';

import models from '../../models';
import { isAuthenticated } from '../resolvers';
import messageType, { messageDefinitions } from './../types/messageType';
import pubsub from '../pubsub';

const sendMessageMutation = mutationWithClientMutationId({
  name: 'SendMessage',
  inputFields: {
    text: { type: new GraphQLNonNull(GraphQLString) },
    receiverId: { type: new GraphQLNonNull(GraphQLID) }
  },
  outputFields: {
    message: {
      type: messageType,
      async resolve({ message }) {
        return message;
      }
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async (
      { text, receiverId, clientMutationId },
      { models: { Message }, user }
    ) => {
      const to = fromGlobalId(receiverId).id;

      const message = await Message.create({
        text,
        from: user.id,
        to
      });

      pubsub.publish('NEW_MESSAGE', { message: message.dataValues });

      return { message };
    }
  )
});

export default sendMessageMutation;
