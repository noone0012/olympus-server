import {
  GraphQLNonNull,
  GraphQLID,
  GraphQLBoolean,
  GraphQLString,
  GraphQLList
} from 'graphql';
import {
  mutationWithClientMutationId,
  fromGlobalId,
  cursorForObjectInConnection
} from 'graphql-relay';

import { isAuthenticated } from '../resolvers';
import fileType from './../inputs/fileType';
import photoType, { photoDefinitions } from './../types/photoType';
import { getAlbumPhotos } from '../../service/rawQueries';
import { upload } from './../../service/s3';

const addPhotosMutation = mutationWithClientMutationId({
  name: 'AddPhotos',
  inputFields: {
    files: { type: new GraphQLList(fileType) },
    albumId: { type: new GraphQLNonNull(GraphQLID) }
  },
  outputFields: {
    photosEdge: {
      type: new GraphQLList(photoDefinitions.edgeType),
      async resolve({ addedPhotos, user, album }) {
        const photos = await getAlbumPhotos(user, album);

        return addedPhotos.map(photo => ({
          cursor: cursorForObjectInConnection(
            photos,
            photos.find(p => p.id === photo.id)
          ),
          node: photo
        }));
      }
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async ({ albumId, files }, { models: { Photo, Album }, user }) => {
      albumId = fromGlobalId(albumId).id;
      const album = await Album.findById(albumId);

      if (!album) {
        throw new Error('Album not found');
      }

      let photos = await Promise.all(
        files.map(file => {
          const photoKey = `${user.email}/photos/${album.name}/${Date.now()}.${
            file.ext
          }`;

          return upload({
            file,
            Key: photoKey
          });
        })
      );

      const addedPhotos = await Photo.bulkCreate(
        photos.map(photo => ({ path: photo.Key, albumId })),
        { returning: true }
      );

      return {
        addedPhotos,
        user,
        album
      };
    }
  )
});

export default addPhotosMutation;
