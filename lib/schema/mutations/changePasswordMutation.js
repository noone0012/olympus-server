import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLBoolean
} from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';

import { comparePassword } from '../../service/password';
import { isAuthenticated } from '../resolvers';
import { generateRefreshToken } from '../../service/auth';

const changePasswordMutation = mutationWithClientMutationId({
  name: 'ChangePassword',
  inputFields: {
    currentPassword: { type: new GraphQLNonNull(GraphQLString) },
    newPassword: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    message: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: payload => payload.message
    },
    refreshToken: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: payload => payload.refreshToken
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async (
      { currentPassword, newPassword },
      { models: { User }, user: { id } }
    ) => {
      const user = await User.findByIdAndCheck(id);
      await comparePassword(currentPassword, user.password);

      user.password = newPassword;
      await user.save();

      return {
        message: `Your password has been successfully changed!`,
        refreshToken: generateRefreshToken({ user })
      };
    }
  )
});

export default changePasswordMutation;
