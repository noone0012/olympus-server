import { GraphQLNonNull, GraphQLString, GraphQLBoolean } from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';
import { fromGlobalId } from 'graphql-relay';

import genderEnum from '../enums/genderEnum';
import statusEnum from '../enums/statusEnum';
import friendAccessEnum from '../enums/friendAccessEnum';
import viewPostsAccessEnum from '../enums/viewPostsAccessEnum';
import viewerType from '../types/viewerType';
import { isAuthenticated } from '../resolvers';

const editUserMutation = mutationWithClientMutationId({
  name: 'EditUser',
  inputFields: {
    email: { type: GraphQLString },
    firstName: { type: GraphQLString },
    lastName: { type: GraphQLString },
    birthday: { type: GraphQLString },
    gender: { type: genderEnum },
    website: { type: GraphQLString },
    description: { type: GraphQLString },
    status: { type: statusEnum },
    friendAccess: { type: friendAccessEnum },
    viewPostsAccess: { type: viewPostsAccessEnum },
    chatMessageSound: { type: GraphQLBoolean },
    notificationsEmail: { type: GraphQLBoolean },
    cityId: { type: GraphQLString },
    facebookAccount: { type: GraphQLString },
    twitterAccount: { type: GraphQLString },
    instagramAccount: { type: GraphQLString }
  },
  outputFields: {
    viewer: {
      type: viewerType,
      resolve: payload => payload
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async (args, { models: { User }, user }) => {
      const newArgs = { ...args };

      if (newArgs.cityId) {
        newArgs.cityId = fromGlobalId(newArgs.cityId).id;
      }

      const data = await User.update(newArgs, {
        where: { id: user.id },
        returning: true
      });
      return data[1][0];
    }
  )
});

export default editUserMutation;
