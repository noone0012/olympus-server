import { GraphQLNonNull, GraphQLID, GraphQLBoolean } from 'graphql';
import { mutationWithClientMutationId, fromGlobalId } from 'graphql-relay';

import { isAuthenticated } from '../resolvers';
import pubsub from '../pubsub';

const sendTypingNotification = mutationWithClientMutationId({
  name: 'SendTypingNotification',
  inputFields: {
    userId: { type: new GraphQLNonNull(GraphQLID) }
  },
  outputFields: {
    success: {
      type: new GraphQLNonNull(GraphQLBoolean),
      resolve: payload => payload.success
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async ({ userId }, { user }) => {
      const to = Number(fromGlobalId(userId).id);
      pubsub.publish('IS_TYPING', { from: user.id, to });

      return { success: true };
    }
  )
});

export default sendTypingNotification;
