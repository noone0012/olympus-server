import { GraphQLNonNull, GraphQLString } from 'graphql';
import {
  mutationWithClientMutationId,
  cursorForObjectInConnection
} from 'graphql-relay';

import models from '../../models';
import { isAuthenticated } from '../resolvers';
import { albumDefinitions } from './../types/albumType';

const createAlbumMutation = mutationWithClientMutationId({
  name: 'CreateAlbum',
  inputFields: {
    name: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    albumEdge: {
      type: albumDefinitions.edgeType,
      async resolve(album) {
        const albums = await models.Album.findAll({
          where: { userId: album.userId },
          raw: true
        });

        return {
          cursor: cursorForObjectInConnection(
            albums,
            albums.find(a => a.id === album.id)
          ),
          node: album.dataValues
        };
      }
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async ({ name }, { models: { Album }, user }) => {
      const album = await Album.findOne({
        where: { name, userId: user.id }
      });

      if (album) {
        throw new Error(`You already have album named "${name}"`);
      }

      return Album.create({ name, userId: user.id });
    }
  )
});

export default createAlbumMutation;
