import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLID,
  GraphQLList,
  GraphQLInputObjectType
} from 'graphql';
import { mutationWithClientMutationId, fromGlobalId } from 'graphql-relay';

import { isAuthenticated } from '../resolvers';
import careerType from '../types/careerType';

const careersInput = new GraphQLInputObjectType({
  name: 'EditCareersFieldInput',
  fields: {
    title: { type: GraphQLString },
    from: { type: GraphQLInt },
    to: { type: GraphQLInt },
    description: { type: GraphQLString },
    id: { type: new GraphQLNonNull(GraphQLID) }
  }
});

const editCareerMutation = mutationWithClientMutationId({
  name: 'EditCareers',
  inputFields: {
    careers: { type: new GraphQLList(new GraphQLNonNull(careersInput)) }
  },
  outputFields: {
    editedCareers: {
      type: new GraphQLList(new GraphQLNonNull(careerType)),
      resolve: payload => payload
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async (args, { models: { Career }, user }) => {
      let editedData = Promise.all(
        args.careers.map(career => {
          const id = fromGlobalId(career.id).id;
          delete career.id;

          return Career.update(career, {
            where: { id, userId: user.id },
            returning: true
          });
        })
      );

      return editedData.then(data => data.map(d => d[1][0]));
    }
  )
});

export default editCareerMutation;
