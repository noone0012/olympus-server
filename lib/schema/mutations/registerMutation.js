import { GraphQLNonNull, GraphQLString } from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';

import genderEnum from '../enums/genderEnum';
import { generateEmailToken } from '../../service/auth';
import mailer from '../../service/mailer';

const registerMutation = mutationWithClientMutationId({
  name: 'Register',
  inputFields: {
    firstName: { type: new GraphQLNonNull(GraphQLString) },
    lastName: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) },
    email: { type: new GraphQLNonNull(GraphQLString) },
    birthday: { type: new GraphQLNonNull(GraphQLString) },
    gender: { type: genderEnum }
  },
  outputFields: {
    message: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: payload => payload.message
    }
  },
  async mutateAndGetPayload(input, { models: { User }, host }) {
    const taken = await User.findOne({ where: { email: input.email } });

    if (taken) {
      throw {
        key: 'email',
        message: `Another account is using ${taken.email}.`
      };
    }

    const user = await User.create(input);

    const token = generateEmailToken({ user });
    const url = `http://${host}/confirmation/${token}`;
    const html = `Click <a href=${url}>here</a> to confirm your account`;

    mailer({ to: user.email, subject: 'Email confirmation', html });

    return {
      message: `Confirmation message has been sent to ${
        user.email
      }. Please confirm it.`
    };
  }
});

export default registerMutation;
