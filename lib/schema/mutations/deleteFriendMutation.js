import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLID,
  GraphQLBoolean
} from 'graphql';
import { mutationWithClientMutationId, fromGlobalId } from 'graphql-relay';

import { isAuthenticated } from '../resolvers';
import friendshipStatusEnum from '../enums/friendshipStatusEnum';

const deleteFriendMutation = mutationWithClientMutationId({
  name: 'DeleteFriend',
  inputFields: {
    userId: { type: new GraphQLNonNull(GraphQLID) }
  },
  outputFields: {
    friendshipStatus: {
      type: new GraphQLNonNull(friendshipStatusEnum),
      resolve: payload => payload.friendshipStatus
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async (args, { models: { User, Friendship, Sequelize: { Op } }, user }) => {
      args.userId = fromGlobalId(args.userId).id;

      if (user.id === args.userId) {
        throw new Error('You cannot delete yourself from friends');
      }

      const friendship = await Friendship.findOneEitherAsRequesterOrAccepter(
        user.id,
        args.userId
      );

      if (!friendship) {
        throw new Error('No such friend request.');
      }

      await friendship.destroy();

      return { friendshipStatus: '0' };
    }
  )
});

export default deleteFriendMutation;
