import {
  GraphQLNonNull,
  GraphQLID,
  GraphQLBoolean,
  GraphQLList
} from 'graphql';
import { mutationWithClientMutationId, fromGlobalId } from 'graphql-relay';

import { isAuthenticated } from '../resolvers';

const deleteCareersMutation = mutationWithClientMutationId({
  name: 'DeleteCareers',
  inputFields: {
    careerIds: { type: new GraphQLList(new GraphQLNonNull(GraphQLID)) }
  },
  outputFields: {
    deletedIds: {
      type: new GraphQLList(new GraphQLNonNull(GraphQLID)),
      resolve: payload => payload
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async ({ careerIds }, { models: { Career, Sequelize: { Op } }, user }) => {
      await Career.destroy({
        where: {
          id: { [Op.in]: careerIds.map(id => fromGlobalId(id).id) },
          userId: user.id
        }
      });

      return careerIds;
    }
  )
});

export default deleteCareersMutation;
