import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLInputObjectType,
  GraphQLList
} from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';

import { isAuthenticated } from '../resolvers';
import careerType from '../types/careerType';
import careerKindEnum from './../enums/careerKindEnum';

const careersInput = new GraphQLInputObjectType({
  name: 'AddCareersFieldInput',
  fields: {
    type: { type: new GraphQLNonNull(careerKindEnum) },
    title: { type: new GraphQLNonNull(GraphQLString) },
    from: { type: new GraphQLNonNull(GraphQLInt) },
    to: { type: new GraphQLNonNull(GraphQLInt) },
    description: { type: GraphQLString }
  }
});

const addCareersMutation = mutationWithClientMutationId({
  name: 'AddCareers',
  inputFields: {
    careers: { type: new GraphQLList(new GraphQLNonNull(careersInput)) }
  },
  outputFields: {
    addedCareers: {
      type: new GraphQLList(new GraphQLNonNull(careerType)),
      resolve: payload => payload
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async (args, { models: { Career }, user }) => {
      const careers = args.careers.map(career => ({
        ...career,
        userId: user.id
      }));

      return Career.bulkCreate(careers, { returning: true });
    }
  )
});

export default addCareersMutation;
