import { GraphQLNonNull, GraphQLString, GraphQLList } from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';

import fileType from '../inputs/fileType';
import userType from '../types/userType';
import { isAuthenticated } from '../resolvers';
import { upload } from '../../service/s3';

const changeHeaderPhotoMutation = mutationWithClientMutationId({
  name: 'ChangeHeaderPhoto',
  inputFields: {
    files: { type: new GraphQLList(fileType) }
  },
  outputFields: {
    viewer: {
      type: userType,
      resolve: payload => payload
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async (args, { models: { User }, user }) => {
      const photoKey = `${user.email}/photos/header-photo`;

      const { Key } = await upload({
        file: args.files[0],
        Key: photoKey
      });

      const data = await User.update(
        { headerPhoto: Key },
        {
          where: { id: user.id },
          returning: true
        }
      );

      return data[1][0];
    }
  )
});

export default changeHeaderPhotoMutation;
