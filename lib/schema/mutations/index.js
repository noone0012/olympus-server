import { GraphQLObjectType } from 'graphql';

import registerMutation from './registerMutation';
import loginMutation from './loginMutation';
import editUserMutation from './editUserMutation';
import changeProfilePhotoMutation from './changeProfilePhotoMutation';
import changeHeaderPhotoMutation from './changeHeaderPhotoMutation';
import changePasswordMutation from './changePasswordMutation';
import addFriendMutation from './addFriendMutation';
import deleteFriendMutation from './deleteFriendMutation';
import addCareersMutation from './addCareersMutation';
import editCareersMutation from './editCareersMutation';
import deleteCareersMutation from './deleteCareersMutation';
import createAlbumMutation from './createAlbumMutation';
import addPhotosMutation from './addPhotosMutation';
import sendMessageMutation from './sendMessageMutation';
import sendTypingNotificationMutation from './sendTypingNotificationMutation';
import markFriendshipAsViewedMutation from './markFriendshipAsViewedMutation';

export default new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    register: registerMutation,
    login: loginMutation,
    editUser: editUserMutation,
    changeProfilePhoto: changeProfilePhotoMutation,
    changeHeaderPhoto: changeHeaderPhotoMutation,
    changePassword: changePasswordMutation,
    addFriend: addFriendMutation,
    deleteFriend: deleteFriendMutation,
    addCareers: addCareersMutation,
    editCareers: editCareersMutation,
    deleteCareers: deleteCareersMutation,
    createAlbum: createAlbumMutation,
    addPhotos: addPhotosMutation,
    sendMessage: sendMessageMutation,
    sendTypingNotification: sendTypingNotificationMutation,
    markFriendshipAsViewed: markFriendshipAsViewedMutation
  }
});
