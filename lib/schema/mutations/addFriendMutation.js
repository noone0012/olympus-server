import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLID,
  GraphQLBoolean
} from 'graphql';
import { mutationWithClientMutationId, fromGlobalId } from 'graphql-relay';

import { isAuthenticated } from '../resolvers';
import friendshipStatusEnum from '../enums/friendshipStatusEnum';

const addFriendMutation = mutationWithClientMutationId({
  name: 'AddFriend',
  inputFields: {
    userId: { type: new GraphQLNonNull(GraphQLID) }
  },
  outputFields: {
    friendshipStatus: {
      type: new GraphQLNonNull(friendshipStatusEnum),
      resolve: payload => payload.friendshipStatus
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async (args, { models: { User, Friendship, Sequelize: { Op } }, user }) => {
      args.userId = fromGlobalId(args.userId).id;

      if (user.id === args.userId) {
        throw new Error('You cannot add yourself to friends');
      }

      const friendship = await Friendship.findOneEitherAsRequesterOrAccepter(
        user.id,
        args.userId
      );

      if (friendship) {
        if (friendship.accepted) {
          throw new Error('You are already friends.');
        }

        friendship.accepted = true;
        await friendship.save();

        return { friendshipStatus: '3' };
      }

      await Friendship.create({
        requesterId: user.id,
        accepterId: args.userId
      });

      return { friendshipStatus: '1' };
    }
  )
});

export default addFriendMutation;
