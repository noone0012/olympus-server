import { GraphQLNonNull, GraphQLID } from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';
import { fromGlobalId } from 'graphql-relay';

import friendshipType from '../types/friendshipType';
import { isAuthenticated } from '../resolvers';

const markFriendshipAsViewedMutation = mutationWithClientMutationId({
  name: 'MarkFriendshipAsViewed',
  inputFields: {
    userId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    friendship: {
      type: new GraphQLNonNull(friendshipType),
      resolve: payload => payload
    }
  },
  mutateAndGetPayload: isAuthenticated.createResolver(
    async ({ userId }, { models: { Friendship }, user }) => {
      const data = Friendship.update(
        { viewed: true },
        {
          where: { requesterId: userId, accepterId: user.id },
          returning: true
        }
      );

      return data[1][0];
    }
  )
});

export default markFriendshipAsViewedMutation;
