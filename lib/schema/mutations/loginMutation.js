import { GraphQLNonNull, GraphQLString } from 'graphql';
import { mutationWithClientMutationId } from 'graphql-relay';

import { tryLogin } from '../../service/auth';

const loginMutation = mutationWithClientMutationId({
  name: 'Login',
  inputFields: {
    email: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    accessToken: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: payload => payload.accessToken
    },
    refreshToken: {
      type: new GraphQLNonNull(GraphQLString),
      resolve: payload => payload.refreshToken
    }
  },
  async mutateAndGetPayload({ email, password }, { models }) {
    return tryLogin({ email, password, models });
  }
});

export default loginMutation;
