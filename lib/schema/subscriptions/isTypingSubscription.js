import { GraphQLNonNull, GraphQLID, GraphQLBoolean } from 'graphql';
import { fromGlobalId } from 'graphql-relay';

import pubsub from '../pubsub';
import { withFilter } from 'graphql-subscriptions';
import { subscriptionWithClientId } from '../subscriptionWithClientId';

const isTypingSubscription = subscriptionWithClientId({
  name: 'IsTyping',
  inputFields: {
    userId: { type: new GraphQLNonNull(GraphQLID) }
  },
  outputFields: {
    isTyping: {
      type: new GraphQLNonNull(GraphQLBoolean),
      resolve: payload => payload.isTyping
    }
  },
  subscribe: withFilter(
    () => pubsub.asyncIterator('IS_TYPING'),
    ({ from, to }, args, { user }) => {
      const subscribedTo = Number(fromGlobalId(args.userId).id);
      const viewer = user.id;

      return from === subscribedTo && to == viewer;
    }
  ),
  getPayload() {
    return { isTyping: true };
  }
});

export default isTypingSubscription;
