import { GraphQLNonNull, GraphQLID, GraphQLString } from 'graphql';
import { fromGlobalId } from 'graphql-relay';

import pubsub from '../pubsub';
import { withFilter } from 'graphql-subscriptions';
import { subscriptionWithClientId } from '../subscriptionWithClientId';

import messageType from './../types/messageType';

const newMessageSubscription = subscriptionWithClientId({
  name: 'NewMessage',
  inputFields: {
    userId: { type: new GraphQLNonNull(GraphQLID) }
  },
  outputFields: {
    message: {
      type: messageType,
      resolve: payload => payload.message
    }
  },
  subscribe: withFilter(
    () => pubsub.asyncIterator('NEW_MESSAGE'),
    ({ message }, args, { user }) => {
      const { from, to } = message;
      const subscribedTo = Number(fromGlobalId(args.userId).id);
      const viewer = user.id;

      return from === subscribedTo && to == viewer;
    }
  )
});

export default newMessageSubscription;
