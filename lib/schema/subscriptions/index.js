import { GraphQLObjectType } from 'graphql';
import newMessageSubscription from './newMessageSubscription';
import isTypingSubscription from './isTypingSubscription';

export default new GraphQLObjectType({
  name: 'Subscription',
  fields: {
    newMessage: newMessageSubscription,
    isTyping: isTypingSubscription
  }
});
