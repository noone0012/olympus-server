import { createResolver } from 'apollo-resolvers';

export const isAuthenticated = createResolver((obj, args, context) => {
  // mutateAndGetPayload
  if (
    context &&
    context.operation &&
    context.operation.operation === 'mutation'
  ) {
    context = args;
    args = obj;
  }

  if (!context.user) {
    throw { key: '_authError', message: 'Invalid token.' };
  }
});
