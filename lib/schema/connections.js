import { GraphQLInt, GraphQLNonNull, GraphQLObjectType } from 'graphql';
import {
  Connection,
  offsetToCursor,
  getOffsetWithDefault,
  cursorToOffset,
  ConnectionArguments,
  connectionDefinitions
} from 'graphql-relay';

type CreateConnectionArgs = {
  name: string,
  type: GraphQLObjectType
};

export function createConnection(args: CreateConnectionArgs) {
  const { name, type } = args;

  return connectionDefinitions({
    name,
    nodeType: type,
    connectionFields: {
      totalCount: {
        type: new GraphQLNonNull(GraphQLInt),
        resolve: conn => conn.totalCount,
        description: 'A count of the total number of objects.'
      }
    }
  });
}

export function connectionFromPaginatedResult<T>(
  arraySlice: Array<T>,
  pagingParameters: PagingParameters
): Connection<T> {
  const { limit, offset, isForwardPaging, isBackwardPaging } = pagingParameters;
  const totalCount = !!arraySlice[0] ? arraySlice[0].fullCount : 0;

  const edges = arraySlice.map((value, index) => ({
    cursor: offsetToCursor(offset + index + 1),
    node: value
  }));

  if (isBackwardPaging) {
    edges.reverse();
  }

  const firstEdge = edges[0];
  const startCursor = firstEdge ? firstEdge.cursor : null;
  const lastEdge = edges[edges.length - 1];
  const endCursor = lastEdge ? lastEdge.cursor : null;

  let hasNextPage;
  let hasPreviousPage;

  if (isForwardPaging) {
    hasNextPage = offset + arraySlice.length < totalCount;
    hasPreviousPage = offset > 0;
  } else {
    hasNextPage = offset > 0;
    hasPreviousPage = offset + arraySlice.length < totalCount;
  }

  return {
    edges,
    pageInfo: {
      startCursor,
      endCursor,
      hasPreviousPage,
      hasNextPage
    },
    totalCount: totalCount
  };
}

export type PagingParameters = {
  limit: number,
  offset: number,
  isForwardPaging: boolean,
  isBackwardPaging: boolean,
  meta: Object
};

export function getPagingParameters(
  args: ConnectionArguments
): PagingParameters {
  const { after, before, first, last, ...meta } = args;

  const isForwardPaging = !!first || !!after;
  const isBackwardPaging = !!last || !!before;

  if (isForwardPaging && isBackwardPaging) {
    throw new Error('cursor-based pagination cannot be forwards AND backwards');
  }
  if ((isForwardPaging && before) || (isBackwardPaging && after)) {
    throw new Error('paging must use either first/after or last/before');
  }
  if ((isForwardPaging && first < 0) || (isBackwardPaging && last < 0)) {
    throw new Error('paging limit must be positive');
  }

  let limit;
  let offset;

  if (isForwardPaging) {
    offset = getOffsetWithDefault(after, 0);
    limit = first;
  } else {
    limit = last;
    offset = getOffsetWithDefault(before, 0);
  }

  return { limit, offset, isForwardPaging, isBackwardPaging, meta };
}
