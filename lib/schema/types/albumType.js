import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLID
} from 'graphql';
import {
  globalIdField,
  connectionArgs,
  connectionFromArray
} from 'graphql-relay';

import { nodeInterface } from '../node';
import photoType, { photoDefinitions } from './photoType';
import { createConnection } from './../connections';
import { getAlbumPhotos } from '../../service/rawQueries';

const albumType = new GraphQLObjectType({
  name: 'Album',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    name: { type: new GraphQLNonNull(GraphQLString) },
    photos: {
      type: photoDefinitions.connectionType,
      args: connectionArgs,
      async resolve(album, args, { models: { sequelize }, user }) {
        const photos = await getAlbumPhotos(user, album);

        return {
          ...connectionFromArray(photos, args),
          totalCount: photos.length
        };
      }
    }
  })
});

export const albumDefinitions = createConnection({ type: albumType });
export default albumType;
