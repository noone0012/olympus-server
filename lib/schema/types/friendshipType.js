import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean
} from 'graphql';
import { globalIdField } from 'graphql-relay';

import { nodeInterface } from '../node';
import { createConnection } from '../connections';
import userType from './userType';

const friendshipType = new GraphQLObjectType({
  name: 'Friendship',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    viewed: { type: new GraphQLNonNull(GraphQLBoolean) },
    accepted: {
      type: new GraphQLNonNull(GraphQLBoolean)
    },
    requester: {
      type: new GraphQLNonNull(userType),
      async resolve({ requesterId }, args, { models: { User } }) {
        return User.findByIdAndCheck(requesterId);
      }
    },
    accepter: {
      type: new GraphQLNonNull(userType),
      async resolve({ accepterId }, args, { models: { User } }) {
        return User.findByIdAndCheck(accepterId);
      }
    }
  })
});

export const friendshipDefinitions = createConnection({ type: friendshipType });
export default friendshipType;
