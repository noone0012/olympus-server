import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean,
  GraphQLList,
  GraphQLInt
} from 'graphql';

import { globalIdField, connectionArgs } from 'graphql-relay';

import genderEnum from '../enums/genderEnum';
import statusEnum from '../enums/genderEnum';
import friendAccessEnum from '../enums/friendAccessEnum';
import viewPostsAccessEnum from '../enums/viewPostsAccessEnum';

import cityType from './cityType';
import countryType from './countryType';
import careerType from './careerType';
import albumType, { albumDefinitions } from './albumType';
import photoType, { photoDefinitions } from './photoType';
import { userDefinitions } from './userType';
import {
  getPagingParameters,
  connectionFromPaginatedResult
} from '../connections';
import type { PagingParameters } from '../connections';
import { getAlbums, getFriends } from '../../service/rawQueries';

export default {
  id: globalIdField(),
  firstName: { type: new GraphQLNonNull(GraphQLString) },
  lastName: { type: new GraphQLNonNull(GraphQLString) },
  fullName: {
    type: new GraphQLNonNull(GraphQLString),
    resolve(user) {
      return `${user.firstName} ${user.lastName}`;
    }
  },
  email: { type: new GraphQLNonNull(GraphQLString) },
  website: { type: GraphQLString },
  birthday: { type: new GraphQLNonNull(GraphQLString) },
  gender: { type: genderEnum },
  profilePhoto: {
    type: GraphQLString,
    resolve({ profilePhoto }, args, { host }) {
      return !profilePhoto
        ? `http://${host}/files/profile-default.png`
        : `https://olympus-0012.s3.amazonaws.com/${profilePhoto}`;
    }
  },
  headerPhoto: {
    type: GraphQLString,
    resolve({ headerPhoto }, args, { host }) {
      return !headerPhoto
        ? `http://${host}/files/header-default.png`
        : `https://olympus-0012.s3.amazonaws.com/${headerPhoto}`;
    }
  },
  description: { type: GraphQLString },
  status: { type: statusEnum },
  joined: { type: new GraphQLNonNull(GraphQLString) },
  facebookAccount: { type: GraphQLString },
  twitterAccount: { type: GraphQLString },
  instagramAccount: { type: GraphQLString },
  friendAccess: { type: new GraphQLNonNull(friendAccessEnum) },
  viewPostsAccess: { type: new GraphQLNonNull(viewPostsAccessEnum) },
  city: {
    type: cityType,
    resolve(user, args, { models: { City } }) {
      return City.findById(user.cityId);
    }
  },
  country: {
    type: countryType,
    async resolve(user, args, { models: { City, Country } }) {
      const city = await City.findById(user.cityId);

      if (!city) {
        return null;
      }

      return Country.findById(city.countryId);
    }
  },
  careers: {
    type: new GraphQLList(new GraphQLNonNull(careerType)),
    resolve(obj, args, { models: { User, Career, Sequelize: { Op } }, user }) {
      return Career.findAll({ where: { userId: user.id } });
    }
  },
  albums: {
    type: albumDefinitions.connectionType,
    args: connectionArgs,
    async resolve(obj, args, { models: { Album }, user }) {
      const pagingParameters: PagingParameters = getPagingParameters(args);

      const albums = await getAlbums({ viewerId: user.id }, pagingParameters);

      return connectionFromPaginatedResult(albums, pagingParameters);
    }
  },
  //   photos: {
  //     type: photoDefinitions.connectionType,
  //     args: connectionArgs,
  //     async resolve(obj, args, { models: { Album, sequelize }, user }) {
  //       const [photos] = await sequelize.query(
  //         `
  //           SELECT * FROM "Users" AS u
  //           INNER JOIN "Albums" AS al
  //           ON u.id = :userId AND u.id = al.user_id
  //           INNER JOIN "Photos" AS ph
  //           ON al.id = ph.album_id
  //         `,
  //         {
  //           replacements: {
  //             userId: user.id
  //           }
  //         }
  //       );

  //       return {
  //         ...connectionFromArray(photos, args),
  //         totalCount: photos.length
  //       };
  //     }
  //   },
  friends: {
    type: userDefinitions.connectionType,
    args: {
      ...connectionArgs,
      fullName: { type: GraphQLString }
    },
    async resolve(
      user,
      args,
      { models: { User, Friendship, Sequelize: { Op }, sequelize } }
    ) {
      args.fullName = args.fullName || '';
      args.fullName = decodeURIComponent(args.fullName);

      const [firstName = '', lastName = ''] = args.fullName
        .split(' ')
        .filter(v => v !== '');

      const pagingParameters: PagingParameters = getPagingParameters(args);

      const friends = await getFriends(
        {
          firstName: `${firstName}%`,
          lastName: `${lastName}%`,
          userId: user.id
        },
        pagingParameters
      );

      return connectionFromPaginatedResult(friends, pagingParameters);
    }
  }
};
