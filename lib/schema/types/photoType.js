import { GraphQLObjectType, GraphQLNonNull, GraphQLString } from 'graphql';
import { globalIdField } from 'graphql-relay';

import { nodeInterface } from '../node';
import { createConnection } from './../connections';

const photoType = new GraphQLObjectType({
  name: 'Photo',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    url: {
      type: new GraphQLNonNull(GraphQLString),
      resolve(photo) {
        return `https://olympus-0012.s3.amazonaws.com/${photo.path}`;
      }
    }
  })
});

export const photoDefinitions = createConnection({ type: photoType });
export default photoType;
