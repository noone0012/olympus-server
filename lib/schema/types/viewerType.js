import {
  GraphQLObjectType,
  GraphQLEnumType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean
} from 'graphql';
import { connectionArgs } from 'graphql-relay';

import { friendshipDefinitions } from './friendshipType';
import { getIncomingRequests } from '../../service/rawQueries';
import { nodeInterface } from '../node';
import {
  getPagingParameters,
  connectionFromPaginatedResult
} from '../connections';
import type { PagingParameters } from '../connections';
import orderEnum from '../enums/orderEnum';

const viewerType = new GraphQLObjectType({
  name: 'Viewer',
  interfaces: [nodeInterface],
  fields: () => {
    const commonUserFields = require('./commonUserFields').default;

    return {
      ...commonUserFields,
      chatMessageSound: { type: new GraphQLNonNull(GraphQLBoolean) },
      notificationsEmail: { type: new GraphQLNonNull(GraphQLBoolean) },
      incomingRequests: {
        type: friendshipDefinitions.connectionType,
        args: {
          ...connectionArgs,
          order: { type: orderEnum }
        },
        async resolve(obj, args, { models: { Friendship }, user }) {
          const pagingParameters: PagingParameters = getPagingParameters(args);

          const incomingRequests = await getIncomingRequests(
            { viewerId: user.id },
            pagingParameters
          );

          return connectionFromPaginatedResult(
            incomingRequests,
            pagingParameters
          );
        }
      }
    };
  }
});

export default viewerType;
