import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLBoolean
} from 'graphql';
import { connectionArgs } from 'graphql-relay';

import { nodeInterface } from '../node';
import {
  createConnection,
  getPagingParameters,
  connectionFromPaginatedResult
} from '../connections';
import type { PagingParameters } from '../connections';
import { getFriends } from '../../service/rawQueries';
import friendshipType from './friendshipType';
import friendshipStatusEnum from '../enums/friendshipStatusEnum';

const userType = new GraphQLObjectType({
  name: 'User',
  interfaces: [nodeInterface],
  fields: () => {
    const commonUserFields = require('./commonUserFields').default;

    return {
      ...commonUserFields,
      isViewer: {
        type: new GraphQLNonNull(GraphQLBoolean),
        resolve(obj, args, { user }) {
          return obj.id === user.id;
        }
      },
      areFriends: {
        type: new GraphQLNonNull(friendshipStatusEnum),
        async resolve(obj, args, { models: { Friendship }, user }) {
          const friendship = await Friendship.findOneEitherAsRequesterOrAccepter(
            obj.id,
            user.id
          );

          if (!friendship) {
            return '0';
          }

          if (friendship.accepted) {
            return '3';
          }

          if (
            friendship.requesterId === user.id &&
            friendship.accepterId === obj.id
          ) {
            return '1';
          }

          if (
            friendship.requesterId === obj.id &&
            friendship.accepterId === user.id
          ) {
            return '2';
          }
        }
      },
      friendship: {
        type: friendshipType,
        async resolve(obj, args, { user, models: { Friendship } }) {
          return Friendship.findOneEitherAsRequesterOrAccepter(obj.id, user.id);
        }
      },
      commonFriends: {
        type: userDefinitions.connectionType,
        args: connectionArgs,
        async resolve(obj, args, { models: { Friendship }, user }) {
          const pagingParameters: PagingParameters = getPagingParameters(args);

          const viewerFriends = await getFriends({
            userId: user.id,
            firstName: '%',
            lastName: '%'
          });
          const viewerFriendsIds = viewerFriends.map(f => f.id);

          const userFriends = await getFriends({
            userId: obj.id,
            firstName: '%',
            lastName: '%'
          });
          const userFriendsIds = userFriends.map(f => f.id);

          const commonFriends = [];

          viewerFriendsIds.forEach(id => {
            if (userFriendsIds.includes(id)) {
              const commonFriend = userFriends.find(f => f.id === id);
              commonFriends.push(commonFriend);
            }
          });

          return connectionFromPaginatedResult(commonFriends, pagingParameters);
        }
      }
    };
  }
});

export const userDefinitions = createConnection({ type: userType });
export default userType;
