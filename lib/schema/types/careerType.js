import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt
} from 'graphql';
import { globalIdField } from 'graphql-relay';

import { nodeInterface } from '../node';
import careerKindEnum from '../enums/careerKindEnum';

const careerType = new GraphQLObjectType({
  name: 'Career',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    type: { type: new GraphQLNonNull(careerKindEnum) },
    title: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: GraphQLString },
    from: { type: new GraphQLNonNull(GraphQLString) },
    to: { type: new GraphQLNonNull(GraphQLString) }
  })
});

export default careerType;
