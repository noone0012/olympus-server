import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean,
  GraphQLID
} from 'graphql';
import { globalIdField, fromGlobalId, toGlobalId } from 'graphql-relay';

import userType from './userType';
import { nodeInterface } from '../node';
import { createConnection } from './../connections';

const messageType = new GraphQLObjectType({
  name: 'Message',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    text: { type: new GraphQLNonNull(GraphQLString) },
    createdAt: { type: new GraphQLNonNull(GraphQLString) },
    unread: { type: new GraphQLNonNull(GraphQLBoolean) },
    from: {
      type: new GraphQLNonNull(GraphQLID),
      resolve({ from }) {
        return toGlobalId('User', from);
      }
    },
    to: {
      type: new GraphQLNonNull(GraphQLID),
      resolve({ to }) {
        return toGlobalId('User', to);
      }
    }
  })
});

export const messageDefinitions = createConnection({ type: messageType });
export default messageType;
