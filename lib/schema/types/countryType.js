import { GraphQLObjectType, GraphQLNonNull, GraphQLString } from 'graphql';
import { globalIdField } from 'graphql-relay';

import { nodeInterface } from '../node';

const countryType = new GraphQLObjectType({
  name: 'Country',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    name: { type: new GraphQLNonNull(GraphQLString) }
  })
});

export default countryType;
