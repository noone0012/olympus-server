import { GraphQLObjectType, GraphQLNonNull, GraphQLString } from 'graphql';
import { globalIdField } from 'graphql-relay';

import { nodeInterface } from '../node';

const cityType = new GraphQLObjectType({
  name: 'City',
  interfaces: [nodeInterface],
  fields: () => ({
    id: globalIdField(),
    name: { type: new GraphQLNonNull(GraphQLString) }
  })
});

export default cityType;
