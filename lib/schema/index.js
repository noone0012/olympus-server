import { GraphQLSchema } from 'graphql';
import { addMiddleware } from 'graphql-add-middleware';

import formatErrors from '../utils/formatErrors';
import ValidationError from '../utils/ValidationError';

import mutationType from './mutations';
import queryType from './queries';
import subscriptionType from './subscriptions';

const schema = new GraphQLSchema({
  query: queryType,
  mutation: mutationType,
  subscription: subscriptionType
});

// error middleware
addMiddleware(schema, async (root, args, context, info, next) => {
  try {
    return await next();
  } catch (e) {
    throw new ValidationError(formatErrors(e));
  }
});

export default schema;
