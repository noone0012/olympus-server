export default (sequelize, DataTypes) => {
  var City = sequelize.define('City', {
    name: DataTypes.STRING
  });

  City.associate = ({ Country }) => {
    City.belongsTo(Country, {
      foreignKey: { name: 'countryId', field: 'country_id' }
    });
  };

  return City;
};
