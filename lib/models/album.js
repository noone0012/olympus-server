import { MAX_LENGTH, LENGTH_RANGE } from '../utils/messages';

export default (sequelize, DataTypes) => {
  var Album = sequelize.define(
    'Album',
    {
      name: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [1, 50],
            msg: LENGTH_RANGE(1, 50)
          }
        }
      }
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['name', 'user_id']
        }
      ]
    }
  );

  Album.associate = ({ Photo }) => {
    Album.hasMany(Photo, {
      foreignKey: { name: 'albumId', field: 'album_id' }
    });
  };

  return Album;
};
