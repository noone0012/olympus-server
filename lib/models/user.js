import bcrypt from 'bcryptjs';

import { LENGTH_RANGE, MAX_LENGTH } from '../utils/messages';
import { greaterThen16 } from '../utils/validators';
import models from './index';

const hashPassword = async user => {
  if (!user.changed('password')) {
    return;
  }

  user.password = await bcrypt.hash(user.password, 10);
};

const checkCity = async user => {
  if (!user.changed('cityId')) {
    return;
  }

  const city = await models.City.findById(user.cityId);

  if (!city) {
    throw new Error('City ID is invalid.');
  }
};

export default (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    firstName: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [2, 25],
          msg: LENGTH_RANGE(2, 25)
        }
      }
    },
    lastName: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [2, 25],
          msg: LENGTH_RANGE(2, 25)
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true,
        len: {
          args: [0, 200],
          msg: MAX_LENGTH(200)
        }
      }
    },
    website: {
      type: DataTypes.STRING,
      validate: {
        isUrl: true,
        len: {
          args: [0, 200],
          msg: MAX_LENGTH(200)
        }
      },
      allowNull: true
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [6, 60],
          msg: LENGTH_RANGE(6, 60)
        }
      }
    },
    birthday: {
      type: DataTypes.DATE,
      validate: { greaterThen16 }
    },
    gender: DataTypes.ENUM('0', '1'),
    confirmed: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    joined: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now')
    },
    profilePhoto: {
      type: DataTypes.STRING
    },
    headerPhoto: {
      type: DataTypes.STRING
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        len: {
          args: [0, 200],
          msg: MAX_LENGTH(200)
        }
      }
    },
    status: {
      type: DataTypes.ENUM('0', '1', '2', '3')
    },
    facebookAccount: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [0, 200],
          msg: MAX_LENGTH(200)
        }
      }
    },
    twitterAccount: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [0, 200],
          msg: MAX_LENGTH(200)
        }
      }
    },
    instagramAccount: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [0, 200],
          msg: MAX_LENGTH(200)
        }
      }
    },
    friendAccess: {
      type: DataTypes.ENUM('0', '1'),
      defaultValue: '0'
    },
    viewPostsAccess: {
      type: DataTypes.ENUM('0', '1'),
      defaultValue: '0'
    },
    chatMessageSound: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    notificationsEmail: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  });

  User.findByIdAndCheck = async function(id) {
    const user = await this.findById(id);

    if (!user) {
      throw new Error('Invalid token.');
    }

    if (!user.confirmed) {
      throw new Error('Account is not confirmed yet.');
    }

    return user;
  };

  User.addHook('beforeSave', hashPassword);
  User.addHook('beforeSave', checkCity);

  User.associate = function({ City, Career, Friendship, Album, Message }) {
    User.belongsTo(City, { foreignKey: { name: 'cityId', field: 'city_id' } });
    User.hasMany(Career, { foreignKey: { name: 'userId', field: 'user_id' } });
    User.belongsToMany(User, {
      as: 'Friends',
      through: Friendship,
      foreignKey: { name: 'requesterId', field: 'requester_id' },
      otherKey: { name: 'accepterId', field: 'accepter_id' }
    });
    User.hasMany(Album, { foreignKey: { name: 'userId', field: 'user_id' } });
    User.hasMany(Message, { foreignKey: 'from' });
    User.hasMany(Message, { foreignKey: 'to' });
  };

  return User;
};
