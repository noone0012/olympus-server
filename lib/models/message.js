import { MAX_LENGTH } from '../utils/messages';

export default (sequelize, DataTypes) => {
  var Message = sequelize.define('Message', {
    text: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [1, 200],
          msg: MAX_LENGTH(200)
        }
      }
    },
    unread: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    }
  });

  Message.associate = () => {};

  return Message;
};
