export default (sequelize, DataTypes) => {
  var Country = sequelize.define('Country', {
    name: DataTypes.STRING
  });

  Country.associate = ({ City }) => {
    Country.hasMany(City, {
      foreignKey: { name: 'countryId', field: 'country_id' }
    });
  };

  return Country;
};
