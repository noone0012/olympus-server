import Sequelize from 'sequelize';

export default (sequelize, DataTypes) => {
  var Friendship = sequelize.define('Friendship', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    accepted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    viewed: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    }
  });

  Friendship.findOneEitherAsRequesterOrAccepter = function(firstId, secondId) {
    return this.findOne({
      where: {
        [Sequelize.Op.or]: [
          {
            requesterId: firstId,
            accepterId: secondId
          },
          {
            requesterId: secondId,
            accepterId: firstId
          }
        ]
      }
    });
  };

  Friendship.associate = ({ User }) => {};

  return Friendship;
};
