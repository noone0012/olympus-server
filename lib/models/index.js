import Sequelize from 'sequelize';
import { decamelize, pascalize } from 'humps';
import fs from 'fs';
import path from 'path';

const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];

const db = {};
let sequelize;

if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

// convert camelCase fields to underscore
sequelize.addHook('beforeDefine', attributes => {
  Object.keys(attributes).forEach(key => {
    const column = attributes[key];
    if (typeof column !== 'function' && !column.field) {
      column.field = decamelize(key);
    }
  });
});

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (
      file.indexOf('.') !== 0 &&
      file !== path.basename(__filename) &&
      file.slice(-3) === '.js'
    );
  })
  .forEach(file => {
    var model = sequelize['import'](path.join(__dirname, file));
    db[pascalize(model.name)] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
