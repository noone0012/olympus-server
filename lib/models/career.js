import { MAX_LENGTH, LENGTH_RANGE } from '../utils/messages';

export default (sequelize, DataTypes) => {
  var Career = sequelize.define('Career', {
    type: DataTypes.ENUM('0', '1'),
    title: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [1, 50],
          msg: LENGTH_RANGE(1, 50)
        }
      }
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        len: {
          args: [0, 200],
          msg: MAX_LENGTH(200)
        }
      }
    },
    from: {
      type: DataTypes.INTEGER,
      validate: {
        max: new Date().getFullYear(),
        min: 2000
      }
    },
    to: {
      type: DataTypes.INTEGER,
      validate: {
        max: new Date().getFullYear(),
        min: 2000
      }
    }
  });

  Career.associate = ({ User }) => {
    Career.belongsTo(User, {
      foreignKey: { name: 'userId', field: 'user_id' }
    });
  };

  return Career;
};
