import 'babel-polyfill';
import 'dotenv/config';

import koa from 'koa';
import koaBody from 'koa-bodyparser';
import cors from '@koa/cors';
import serve from 'koa-static';
import mount from 'koa-mount';
import { execute, subscribe } from 'graphql';
import { createServer } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import jwt from 'jsonwebtoken';

import models from './models';
import addUser from './middleware/addUser';
import upload from './middleware/upload';
import graphql from './routes/graphql';
import confirmation from './routes/confirmation';
import refresh_token from './routes/refresh_token';
import schema from './schema';

const app = new koa();

app.use(cors());
app.use(mount('/files', serve('files')));
app.use(koaBody());
app.use(addUser());
app.use(upload());

app.use(graphql.routes());
app.use(graphql.allowedMethods());

app.use(confirmation.routes());
app.use(confirmation.allowedMethods());

app.use(refresh_token.routes());
app.use(refresh_token.allowedMethods());

const force = false;
const PORT = process.env.NODE_ENV === 'production' ? 80 : 1200;

const server = createServer(app.callback());

models.sequelize.sync({ force }).then(() => {
  server.listen(PORT, () => {
    new SubscriptionServer(
      {
        execute,
        subscribe,
        schema,
        async onConnect({ accessToken }) {
          try {
            if (!accessToken) {
              throw new Error();
            }

            let payload = jwt.verify(accessToken, process.env.ACCESS_SECRET);

            return { user: payload.user, models };
          } catch (e) {
            throw new Error('Invalid token.');
          }
        }
      },
      {
        server,
        path: '/subscriptions'
      }
    );

    console.log(`server is listening to port ${PORT}`);
  });
});
