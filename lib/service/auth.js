import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { WRONG_EMAIL } from '../utils/messages';
import { comparePassword } from '../service/password';

const { ACCESS_SECRET, REFRESH_SECRET, EMAIL_SECRET } = process.env;

export const generateEmailToken = ({ user: { id } }) => {
  const payload = { user: { id } };

  return jwt.sign(payload, EMAIL_SECRET);
};

export const generateAccessToken = ({ user: { id, email } }) => {
  const payload = { user: { id, email } };

  return jwt.sign(payload, ACCESS_SECRET, { expiresIn: '1d' });
};

export const generateRefreshToken = ({ user: { id, password } }) => {
  const payload = { user: { id } };
  const refreshSecret = REFRESH_SECRET + password;

  return jwt.sign(payload, refreshSecret, { expiresIn: '7d' });
};

export const tryLogin = async ({ email, password, models: { User } }) => {
  const user = await User.findOne({ where: { email } });

  // bad email
  if (!user) {
    throw {
      key: 'email',
      message: WRONG_EMAIL
    };
  }

  // throw if does not match
  await comparePassword(password, user.password);

  if (!user.confirmed) {
    throw new Error('Account is not confirmed yet');
  }

  return {
    accessToken: generateAccessToken({ user }),
    refreshToken: generateRefreshToken({ user })
  };
};

export const refreshToken = async ({ token, models: { User } }) => {
  const { user: { id } } = jwt.decode(token);

  // bad token
  if (!id) {
    throw new Error('Invalid token');
  }

  const user = await User.findByIdAndCheck(id);

  const refreshSecret = REFRESH_SECRET + user.password;

  jwt.verify(token, refreshSecret);

  const payload = { user: { id } };

  return generateAccessToken({ user });
};
