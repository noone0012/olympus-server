import { camelizeKeys } from 'humps';

import type { PagingParameters } from '../schema/connections';
import { FriendsReplacements } from './rawQueries';
import models from '../models';

const { sequelize } = models;

export default async function(
  query: string,
  replacements: FriendsReplacements,
  pagingParameters: PagingParameters = {}
) {
  const {
    limit,
    offset,
    isForwardPaging,
    isBackwardPaging,
    meta
  } = pagingParameters;
  const limitQuery = limit ? `LIMIT ${limit}` : '';
  const offsetQuery = offset ? `OFFSET ${offset}` : '';
  let order;

  if (meta && meta.order) {
    order = meta.order;
  } else {
    order = isForwardPaging ? 'ASC' : 'DESC';
  }

  const orderQuery = `ORDER BY row_number ${order}`;
  const newQuery = `${query} ${orderQuery} ${limitQuery} ${offsetQuery}`;

  let [results] = await sequelize.query(newQuery, {
    replacements
  });

  return camelizeKeys(results);
}
