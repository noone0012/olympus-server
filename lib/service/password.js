import bcrypt from 'bcryptjs';

import { WRONG_PASSWORD } from '../utils/messages';

export const comparePassword = async (str, hash) => {
  const valid = await bcrypt.compare(str, hash);
  // bad password
  if (!valid) {
    throw {
      key: 'password',
      message: WRONG_PASSWORD
    };
  }
};
