import AWS from 'aws-sdk';
import fs from 'fs';

const bucketName = 'olympus-0012';

const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  params: { Bucket: bucketName }
});

export function upload({ Key, file }) {
  const Body = fs.createReadStream(file.path);

  return s3
    .upload({
      Key,
      Body,
      ACL: 'public-read'
    })
    .promise();
}
