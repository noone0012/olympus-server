import nodemailer from 'nodemailer';

const mailer = ({ to, subject, html }) => {
  // create transporter
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    auth: {
      user: process.env.MAILER_USER,
      pass: process.env.MAILER_PASS
    }
  });

  // create options for sending email
  const mailOptions = {
    from: {
      name: 'Olympus',
      address: process.env.MAILER_USER
    },
    to,
    subject,
    html
  };

  return transporter.sendMail(mailOptions, e => {
    console.log(e);
  });
};

export default mailer;
