import { camelizeKeys } from 'humps';

import paginate from './paginate';
import type { PagingParameters } from '../schema/connections';
import models from '../models';
const sequelize = models.sequelize;

export async function getAlbumPhotos(user, album) {
  const [photos] = await sequelize.query(
    `
        SELECT * FROM "Users" AS u
        INNER JOIN "Albums" AS al
        ON u.id = :userId AND u.id = al.user_id
        INNER JOIN "Photos" AS ph
        ON al.id = :albumId AND al.id = ph.album_id
      `,
    {
      replacements: {
        userId: user.id,
        albumId: album.id
      }
    }
  );

  return photos;
}

type MessagesReplacements = {
  viewerId: number,
  peerId: string
};

export function getMessages(
  replacements: MessagesReplacements,
  pagingParameters: PagingParameters
) {
  const query = `
    SELECT row_number() OVER () AS row_number, count(*) OVER () AS full_count, m.*
    FROM "Messages" AS m
    WHERE (m.from = :viewerId AND m.to = :peerId) OR (m.from = :peerId AND m.to = :viewerId)
`;

  return paginate(query, replacements, pagingParameters);
}

type FriendsReplacements = {
  userId: number,
  firstName: string,
  lastName: string
};

export async function getFriends(
  replacements: FriendsReplacements,
  pagingParameters: PagingParameters
) {
  const query = `
    SELECT row_number() OVER () AS row_number, count(*) OVER () AS full_count, u.*
    FROM "Users" AS u
    INNER JOIN "Friendships" AS f
    ON accepted = true
    AND (requester_id = :userId OR accepter_id = :userId)
    AND u.id IN (requester_id, accepter_id)
    AND u.id != :userId
    AND u.first_name ILIKE :firstName
    AND u.first_name ILIKE :lastName
`;

  return paginate(query, replacements, pagingParameters);
}

type UsersReplacements = {
  firstName: string,
  lastName: string
};

export function getUsers(
  replacements: UsersReplacements,
  pagingParameters: PagingParameters
) {
  const query = `
    SELECT row_number() OVER () AS row_number, count(*) OVER () AS full_count, u.*
    FROM "Users" AS u
    WHERE confirmed = true
    AND first_name ILIKE :firstName
    AND first_name ILIKE :lastName
`;

  return paginate(query, replacements, pagingParameters);
}

type IncomingRequestsReplacements = {
  viewerId: number
};

export function getIncomingRequests(
  replacements: IncomingRequestsReplacements,
  pagingParameters: PagingParameters
) {
  const query = `
    SELECT row_number() OVER () AS row_number, count(*) OVER () AS full_count, f.*
    FROM "Users" AS u
    INNER JOIN "Friendships" AS f
    ON accepter_id = :viewerId
    AND u.id = requester_id
`;

  return paginate(query, replacements, pagingParameters);
}

type AlbumsReplacements = {
  viewerId: number
};

export function getAlbums(
  replacements: AlbumsReplacements,
  pagingParameters: PagingParameters
) {
  const query = `
    SELECT row_number() OVER () AS row_number, count(*) OVER () AS full_count, a.*
    FROM "Albums" AS a
    WHERE a.user_id = :viewerId
`;

  return paginate(query, replacements, pagingParameters);
}
